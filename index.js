/*jshint esnext: true, node: false, sub: true */
{
const link = document.getElementById('link');
const href = document.getElementById('href');
const title = document.getElementById('title');
const zone = document.getElementById('zone');
const info = document.getElementById('info');
const newEntry = document.getElementById('new_entry');
const save = document.getElementById('save');
const send = document.getElementById('send');
const clear = document.getElementById('clear');
const exit = document.getElementById('exit');
const popupNewEntry = document.getElementById('popup_new_entry');

const d = new Date();
let gDate = `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}  `;
let gTime = `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;

let openPopup = ()=>{
	popupNewEntry.style.visibility="visible";
	if(localStorage.hasOwnProperty("entryItem")){
		let E = getObj("entryItem");
		localStorage.removeItem("entryItem");
		link.value = E.link; resizeTextarea(link);
		href.value = E.href; resizeTextarea(href);
		title.value = E.title; resizeTextarea(title);
		zone.value = E.zone; resizeTextarea(zone);
	}
};

let closePopup = ()=>{
	let E = {};
	popupNewEntry.style.visibility="hidden";
	E.link = link.value;
	E.href = href.value;
	E.title = title.value;
	E.zone = zone.value;
	setObj("entryItem", E);
};

let getJson = (url, cb) =>{
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = () =>{
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            cb(JSON.parse(xmlhttp.responseText));
        };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
};

let postJson = (obj)=> {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "/Bookmarks");
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.send(JSON.stringify(obj));
};

let setObj = (item, value) => {
    localStorage.setItem(item, JSON.stringify(value));
};

let getObj = (item) => {
    return JSON.parse(localStorage.getItem(item));
};

let clearText = ()=> {
    localStorage.removeItem("entryItem");
    link.value = ""; resizeTextarea(link);
    href.value = ""; resizeTextarea(href);
    title.value = ""; resizeTextarea(title);
    zone.value = ""; resizeTextarea(zone);
};

let isLink = t=> /^[A-Za-z][A-Za-z -_]*$/g.test(t);
let isHref = t=> /^https?:\/\/[A-Za-z0-9-_.\/?&#:=]*$/g.test(t);
let isTitle = t=> /^[A-Za-z][A-Za-z0-9 ,.;:!?'"\(\)_-]*$/g.test(t);
let isZone = t=> /^[ABCD]{2}$/.test(t);

let saveNewEntry = ()=>{

    let entryQueue = {};
    let bookmarks = getObj("bookmarks");
    let error = "OK";

    if(isLink(link.value) === false)
    error = "malformed link";
    else {
    if(isHref(href.value) === false)
	error = "malformed href";
	else {
    if(isTitle(title.value) === false)
    error = "malformed title";
    else {
    if(isZone(zone.value) === false)
	error = "malformed zone";
	}}}

	if(error === "OK"){

		let entry =
		`{"href":"${href.value}",
		"title":"${title.value}",
		"zone":"${zone.value}"}`;

		if(localStorage.hasOwnProperty("entryQueue"))
			entryQueue = getObj("entryQueue");
		entryQueue[link.value] = JSON.parse(entry);
		setObj("entryQueue", entryQueue);
		bookmarks = getObj("bookmarks");
		bookmarks.data[link.value] = JSON.parse(entry);
		setObj("bookmarks", bookmarks);
		renderLink(bookmarks, link.value);
		displayInfo(bookmarks, error);
    }
    else displayInfo(bookmarks, error);
};

let sendToServer = ()=>{
    if(localStorage.hasOwnProperty("entryQueue")){
		postJson(getObj("entryQueue"));
		localStorage.removeItem("entryQueue");
    }
};

newEntry.addEventListener("click", openPopup);
save.addEventListener("click", saveNewEntry);
send.addEventListener("click", sendToServer);
clear.addEventListener("click", clearText);
exit.addEventListener("click", closePopup);

let resizeTextarea = (textarea)=>{
    textarea.style.height = 'auto';
    textarea.style.height = `${textarea.scrollHeight}px`;
};

link.addEventListener('input', ()=>{resizeTextarea(link);});
href.addEventListener('input', ()=>{resizeTextarea(href);});
title.addEventListener('input', ()=>{resizeTextarea(title);});
zone.addEventListener('input', ()=>{resizeTextarea(zone);});

let displayInfo = (obj, S) =>{

    let T = `Last reload:  ${gDate}${gTime}`;
    let E =  `Number of entries: ${Object.keys(obj.data).length}`;
    info.innerHTML = `(${T})\n(${E})\n${S}`;
};

let renderLink = (obj, key)=> {

	let a = {};
	let link = "";
	let s = String.fromCodePoint(12296);
	let f = String.fromCodePoint(12297);

	link = key.split('\u0020').join('\u00A0');
	a = document.createElement('a');
	a.title = obj.data[key].title;
	a.href = obj.data[key].href;
	a.appendChild(document.createTextNode(` ${s}${link}${f}`));
	document.getElementById(obj.data[key].zone).appendChild(a);
};

let start = (obj) =>{

    displayInfo(obj, "OK");
    let keyList = Object.keys(obj.data);

    for(let index=0; index < keyList.length; index++)
    renderLink(obj, keyList[index]);
};

if(localStorage.hasOwnProperty("bookmarks"))
    start(getObj("bookmarks"));
else
    getJson("bookmarks.json", (obj)=>{setObj("bookmarks", obj); start(obj);});
}

/* Dummy entry for testing. */
let dummyEntry = ()=>{
link.value = "Eloquent Javascript";
href.value = "https://eloquentjavascript.net";
title.value = "This is a book about JavaScript, programming, and the wonders of the digital.";
zone.value = "AA";
};


